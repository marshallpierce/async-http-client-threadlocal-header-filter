package org.mpierce.ahc.threadlocal

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.asynchttpclient.AsyncHttpClient
import org.asynchttpclient.Dsl.asyncHttpClient
import org.asynchttpclient.Dsl.config
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

internal class ThreadLocalHeaderFilterTest {
    private lateinit var server: ApplicationEngine

    private val port = 12000

    private val headerName = "x-call-id"

    private val defaultHeaders = mapOf(
            "host" to listOf("localhost:12000"),
            "accept" to listOf("*/*"),
            "user-agent" to listOf("AHC/2.1")
    )

    private val tl = ThreadLocal.withInitial<String?> { null }
    private val client = config()
            .addRequestFilter(ThreadLocalHeaderFilter(headerName, tl))
            .build()
            .let { asyncHttpClient(it) }

    @BeforeEach
    internal fun setUp() {
        server = embeddedServer(Netty, port = port) {
            install(ContentNegotiation) {
                register(ContentType.Application.Json,
                        JacksonConverter(mapper))
            }
            install(Routing) {
                get("/headers") {
                    call.respond(call.request.headers
                            .entries()
                            .map { Pair(it.key, it.value) }
                            .toMap())
                }
            }
        }
        server.start(wait = false)
    }

    @AfterEach
    internal fun tearDown() {
        server.stop(10, 10, TimeUnit.MILLISECONDS)
        client.close()
    }

    @Test
    internal fun noHeaderWhenNoContext() {
        val reqHeaders = callHeadersEndpoint(client)
        assertEquals(defaultHeaders, reqHeaders)
    }

    @Test
    internal fun sendsHeaderWhenContextPresent() {
        tl.set("foo")
        val reqHeaders = callHeadersEndpoint(client)
        assertEquals(defaultHeaders
                .plus(headerName to listOf("foo")),
                reqHeaders)
    }

    private fun callHeadersEndpoint(client: AsyncHttpClient): Map<String, List<String>> {
        val resp = client.prepareGet(("http://localhost:$port/headers")).execute().get()
        return resp.responseBodyAsStream.let {
            mapper.reader()
                    .forType(object : TypeReference<Map<String, List<String>>>() {})
                    .readValue(it)
        }
    }
}

private val mapper = ObjectMapper().apply {
    registerModule(KotlinModule())
}
