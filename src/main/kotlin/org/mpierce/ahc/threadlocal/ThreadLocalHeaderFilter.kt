package org.mpierce.ahc.threadlocal

import org.asynchttpclient.filter.FilterContext
import org.asynchttpclient.filter.RequestFilter

/**
 * Sets the specified header to the content of the thread local, if the content is non-null.
 *
 * In other words, if the thread local has a non-null value, a header will be added with that value, and if it's null,
 * no header will be added.
 */
class ThreadLocalHeaderFilter(private val headerName: String,
                              private val threadLocal: ThreadLocal<String?>) : RequestFilter {
    override fun <T : Any?> filter(ctx: FilterContext<T>): FilterContext<T> {
        val contents = threadLocal.get()
        if (contents != null) {
            ctx.request.headers.add(headerName, contents)
        }

        return ctx
    }
}
