import com.jfrog.bintray.gradle.BintrayExtension
import java.util.Date

plugins {
    kotlin("jvm") version "1.3.41"
    id("com.github.ben-manes.versions") version "0.21.0"
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.4"
    id("org.jetbrains.dokka") version "0.9.18"
    id("net.researchgate.release") version "2.8.1"
}

repositories {
    jcenter()
}

group = "org.mpierce.async-http-client"

val deps by extra {
    mapOf(
            "junit" to "5.5.0",
            "ktor" to "1.2.2"
    )
}

dependencies {
    implementation(kotlin("stdlib"))

    api("org.asynchttpclient:async-http-client:2.10.1")

    testImplementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-jackson:${deps["ktor"]}")
    testImplementation("com.fasterxml.jackson.core:jackson-databind:2.9.9.1")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.9")
    testRuntime("org.slf4j:slf4j-simple:1.7.26")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks {
    test {
        useJUnitPlatform()

    }

    register<Jar>("sourceJar") {
        from(sourceSets["main"].allSource)
        archiveClassifier.set("sources")
    }

    register<Jar>("docJar") {
        from(project.tasks["dokka"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(bintrayUpload)
    }
}

publishing {
    publications {
        register<MavenPublication>("bintray") {
            from(components["java"])
            artifact(tasks["sourceJar"])
            artifact(tasks["docJar"])
        }
    }

    configure<BintrayExtension> {
        user = rootProject.findProperty("bintrayUser")?.toString()
        key = rootProject.findProperty("bintrayApiKey")?.toString()
        setPublications("bintray")

        with(pkg) {
            repo = "maven"
            setLicenses("Copyfree")
            vcsUrl = "https://bitbucket.org/marshallpierce/async-http-client-threadlocal-header-filter"
            name = "async-http-client-threadlocal-header-filter"

            with(version) {
                name = project.version.toString()
                released = Date().toString()
                vcsTag = project.version.toString()
            }
        }
    }
}
